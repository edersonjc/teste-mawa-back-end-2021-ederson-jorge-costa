<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use \App\User;

class UsuarioCrudController extends Controller
{
    public function addUser(Request $request) {
        $usuario = new User();
        $usuario->nome = $request['nome'];
        $usuario->email = $request['email'];
        $usuario->password = Hash::make($request['senha']);
        $usuario->api_token = Str::random(60);
        $usuario->is_active = false;
        $usuario->save();

        return response()->json(['status' => 'success', 'message' => 'Cadastrado com sucesso!', 'title' => 'Sucesso'], 200);
    }

    public function removeUser(Request $request, User $usuario) {
        $usuario->delete();
        return response()->json(['status' => 'success', 'message' => 'Removido com sucesso!', 'title' => 'Sucesso'], 200);
    }

    public function updateUser(Request $request, User $usuario) {
        if($request['password_confirmation'] != $request['password']) {
            return response()->json(['status' => 'warning', 'message' => 'Confirmação da senha diferente de senha!', 'title' => 'Atenção!'], 200);
        } else {
            $usuario->nome = $request['nome'];
            $usuario->password = Hash::make($request['password']);
            $usuario->update();

            return response()->json(['status' => 'success', 'message' => 'Alterado com sucesso!', 'title' => 'Sucesso'], 200);
        }
    }

    public function updatePerfil(Request $request) {
        return $this->updateUser($request, auth()->user());
    }
}
