<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use App\CotacoesHistorico;

class CotacoesController extends CotacoesCrudController
{
    public function consultar() {
        $list = $this->readMoedas();
        return view('cotacoes.consultar', compact('list'));
    }

    public function cotar(Request $request, $moeda, $moedaPar) {
        $key = $moeda . "-" . $moedaPar;
        $data = json_decode(file_get_contents('http://economia.awesomeapi.com.br/last/' . $key), true);
        $key = str_replace("-","",$key);
        
        $response = $this->addHistorico($data, $key);
        $response = $this->responseToHTML($data, $key);

        return response()->json(['status' => 'success', 'response' => $response], 200);
    }
    
    public function historico() {
        $list = CotacoesHistorico::where("user_id", auth()->user()->id)->orderBy('created_at', 'desc')->paginate(10);
        return view('cotacoes.list', compact('list'));
    }
}
