<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Admin;
use App\Writer;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    use RegistersUsers;
    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nome' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'senha' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function showAdminRegisterForm()
    {
        return view('auth.register', ['url' => 'admin']);
    }

    protected function register(Request $request)
    {
        $user = User::create([
            'nome' => $request['nome'],
            'email' => $request['email'],
            'is_admin' => true,
            'is_active' => true,
            'password' => Hash::make($request['senha'])
        ]);

        if(auth()->attempt(array('email' => $request['email'], 'password' => $request['senha']))) {
            return back()->withErrors([
                'message' => 'Senha ou email estão incorretos!'
            ]);
        }
        
        return redirect()->intended('/home');
    }
}
