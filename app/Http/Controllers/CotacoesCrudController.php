<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CotacoesHistorico;

class CotacoesCrudController extends AwesomeController
{
    public function addHistorico($data, $key) {
        CotacoesHistorico::create([
            'par' => $data[$key]['code']."-".$data[$key]['codein'],
            'compra' => $data[$key]['bid'],
            'venda' => $data[$key]['ask'],
            'variacao' => $data[$key]['varBid'],
            'porcentagem_variacao' => $data[$key]['pctChange'],
            'maximo' => $data[$key]['high'],
            'minimo' => $data[$key]['low'],
            'user_id' => auth()->user()->id
        ]);
    }
}
