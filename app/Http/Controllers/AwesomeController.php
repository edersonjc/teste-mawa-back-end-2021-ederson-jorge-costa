<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AwesomeController extends UtilsController
{
    public function readMoedas() {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', "http://economia.awesomeapi.com.br/xml/available/uniq");
        $xmlpage =  $response->getBody();
        $xml = simplexml_load_string($xmlpage);

        $list = [];
        foreach ($xml as $key => $value) {
            $obj = new \stdClass();
            $obj->id = $key;
            $obj->value = $value;
            
            array_push($list, $obj);
        }

        usort($list, function( $a, $b ) {
            if( $a->id == $b->id ) return 0;
            return ( ( $a->id < $b->id ) ? -1 : 1 );
        });
        
        return $list;
    }

    public function readMoedasPar($moeda) {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', "http://economia.awesomeapi.com.br/xml/available");
        $xmlpage =  $response->getBody();
        $xml = simplexml_load_string($xmlpage);

        $list = [];
        foreach ($xml as $key => $value) {
            $moedaPar = explode("-", $key);
            $descricaoMoedaPar = explode("/", $value);

            $obj = new \stdClass();
            $obj->id = $moedaPar[1];
            $obj->value = $descricaoMoedaPar[1];

            if(Str::startsWith($key, $moeda)) {
                array_push($list, $obj);
            }
        }
        
        usort($list, function( $a, $b ) {
            if( $a->id == $b->id ) return 0;
            return ( ( $a->id < $b->id ) ? -1 : 1 );
        });

        $response = $this->toSelectOption($list);

        return response()->json(['status' => 'success', 'response' => $response], 200);
    }
}
