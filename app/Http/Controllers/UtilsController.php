<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilsController extends Controller
{
    public function responseToHTML($data, $key) {
        $html = "";
        if( sizeof($data) > 0 ) {
            $html .= "<div class=\"table-responsive overflow-x-inherit\">
                <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                        <tr>
                            <th>Par</th>
                            <th>Compra</th>
                            <th>Venda</th>
                            <th>Variação</th>
                            <th>Porcentagem de Variação</th>
                            <th>Máximo</th>
                            <th>Mínimo</th>
                        </tr>
                    </thead>
                    <tbody>";

                    $html .= "<tr>";
                    $html .= "<td>".$data[$key]['code']."-".$data[$key]['codein']."</td>";
                    $html .= "<td>".$data[$key]['bid']."</td>";
                    $html .= "<td>".$data[$key]['ask']."</td>";
                    $html .= "<td>".$data[$key]['varBid']."</td>";
                    $html .= "<td>".$data[$key]['pctChange']."</td>";
                    $html .= "<td>".$data[$key]['high']."</td>";
                    $html .= "<td>".$data[$key]['low']."</td>";
                    $html .= "</tr>";

            $html .= "</tbody>
                </table>
            </div>";
        } else {
            "<p>Nenhuma cotação encontrada</p>";
        }

        return $html;
    }

    public function toSelectOption($list) {
        $html = "<option value=\"\">Selecione</option>";

        foreach ($list as $row) {
            $html .= "<option value=\"$row->id\">$row->id</option>";
        }

        return $html;
    }
}
