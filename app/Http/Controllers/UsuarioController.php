<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\User;
use DB;

class UsuarioController extends UsuarioCrudController
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $params = array('name' => $request->name, 'email' => $request->email);
        $list = User::where('id', '>', 0);

        if ( !empty($request->name) ) {
            $list->where('name', 'like', '%'.$request->name.'%');
        }

        if ( !empty($request->email) ) {
            $list->where('email', 'like', '%'.$request->email.'%');
        }

        $list = $list->paginate(20);
        $usuarios = "active";
        return view('usuario.list', compact('list', 'params', 'usuarios'));
    }

    public function showUser(User $usuario) {
        $usuario = User::find($usuario->id);
        $usuarios = "active";
        return view('usuario.show', compact('usuario', 'usuarios'));
    }

    public function showPerfil() {
        $usuario = auth()->user();
        $tipo_usuario = 'Perfil';
        return view('usuario.perfil', compact('tipo_usuario', 'usuario'));
    }
}