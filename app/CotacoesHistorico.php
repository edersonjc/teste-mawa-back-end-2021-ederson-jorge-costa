<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CotacoesHistorico extends Model
{
    protected $fillable = [
        'par', 'compra', 'venda', 'variacao', 'porcentagem_variacao', 'maximo', 'minimo', 'user_id',
    ];
}
