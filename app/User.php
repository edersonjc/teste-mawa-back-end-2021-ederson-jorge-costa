<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Sistema;
use App\UF;
use App\Comentario;
use App\Parametro;
use App\Debito;
use App\Perfil;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nome', 'email', 'password', 'is_admin', 'nr_documento', 'tipo_pessoa', 'cep', 'endereco',
         'numero', 'bairro', 'cidade', 'uf', 'is_active',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateToken() {
        $this->api_token = Str::random(60);
        $this->save();

        return $this->api_token;
    }

    public function perfils() {
        return $this->belongsToMany(Perfil::class);
    }
    
    public function empresas() {
        return $this->belongsToMany(Empresa::class);
    }
}
