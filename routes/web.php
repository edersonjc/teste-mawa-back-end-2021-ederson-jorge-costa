<?php
Route::view('/', 'welcome');
Auth::routes();

Route::group(['middleware' => ['auth:web']], function () {
    Route::get('/consultar',                        'CotacoesController@consultar')->name('cotacoes.consultar');
    Route::get('/cotar/{moeda}/{moedaPar}',         'CotacoesController@cotar')->name('cotacoes.cotar');
    Route::get('/historico',                        'CotacoesController@historico')->name('cotacoes.historico');
    Route::get('/readMoedas',                       'CotacoesController@readMoedas')->name('moedas.read');
    Route::get('/readMoedasPar/{moeda}',            'CotacoesController@readMoedasPar')->name('moedas.readMoedasPar');
});