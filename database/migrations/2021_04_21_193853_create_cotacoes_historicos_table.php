<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotacoesHistoricosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotacoes_historicos', function (Blueprint $table) {
            $table->id();
            $table->string('par');
            $table->decimal('compra', 10, 2);
            $table->decimal('venda', 10, 2);
            $table->decimal('variacao', 10, 2);
            $table->string('porcentagem_variacao', 10, 9);
            $table->decimal('maximo', 10, 2);
            $table->decimal('minimo', 10, 2);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotacoes_historicos');
    }
}
