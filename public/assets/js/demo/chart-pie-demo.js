// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

var labels = []; //["Pagamento", "Andamento do Pedido", "Entrega", "Troca e Devolução"];
var dados = []; //[25, 20, 15, 40];
var bgColors = []; //['#4e73df', '#1cc88a', '#36b9cc', '#D32F2F'];
var bgHover = []; //'#2e59d9', '#17a673', '#2c9faf', "yellow"

function buscarTipoAtendimento(idEmpresa) {
    //ajax para buscar essa informação
    $.ajax({
        url: '/relatorios/tipos/' + idEmpresa,
        type: 'GET',
        dataType: 'json',
        processData: false,
        contentType: false,
        async: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            console.log(data);
            
            var lista = data.lista;

            $(".empresaSelecionadaTipos").html(data.nomeEmpresa);
    
            var arrayLabels = [];
            var arrayDados = [];
            var bgColors = [];
    
            var i;
            for (i = 0; i < lista.length; i++) {
                arrayLabels.push(lista[i].name)
                arrayDados.push(lista[i].quantidade)
                bgColors.push(lista[i].color)
            }
            
            // Pie Chart Example
            var ctx = document.getElementById("myPieChart");

            var myPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: arrayLabels,
                    datasets: [{
                        data: arrayDados,
                        backgroundColor: bgColors,
                        hoverBackgroundColor: bgHover,
                        hoverBorderColor: "rgba(234, 236, 244, 1)",
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        caretPadding: 10,
                    },
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 80,
                },
            });
        },
        error: function(data) {
            loading();
            console.log(data);

            swal("Ops.. :(", data.responseJSON.message, "warning")
        }
    });
}

buscarTipoAtendimento(0);

$(".btnTiposEmpresa").on("click", function () {
    var idEmpresa = $(this).attr("data-id");
    buscarTipoAtendimento(idEmpresa);
})