$(document).ready(function() {

    $("#moeda").on("change", function() {
        var moeda = this.value;

        if (this.value != "") {
            $.ajax({
                url: '/readMoedasPar/' + moeda,
                type: 'GET',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data)

                    if (data.status == "success") {
                        $("#moedaPar").html(data.response);
                    }
                },
                error: function(data) {
                    swal("Ops.. :(", data.responseJSON.message, "warning")
                }
            })
        }
    })

    $(".form").submit(function(e) {
        e.preventDefault();

        loading();
        $.ajax({
            url: '/cotar/' + $("#moeda").val() + "/" + $("#moedaPar").val(),
            type: 'GET',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                loading();
                console.log(data);

                if (data.status == 'success') {
                    $("#resultado").html(data.response)
                } else {
                    swal('Oops...', data.message, data.status)
                }
            },
            error: function(data) {
                loading();
                swal("Ops.. :(", data.responseJSON.message, "warning")
            }
        })
    })
})