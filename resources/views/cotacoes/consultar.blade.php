@extends('layouts.cliente')

@section('css')
    <link rel="stylesheet" href="/css/cotacoes/consultar.css">
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 pl-0 mb-0 text-gray-800">Cotações</h1>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Consultar</h6>
                </div>
                <div class="card-body">
                    <form class="form">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="moeda">{{ __('Moeda') }}</label> 
                            </div>
                            <div class="col-md-8">
                                <select name="moeda" id="moeda" class="form-control">
                                    <option value="">Selecione</option>
                                    @foreach ($list as $row)
                                        <option value="{{$row->id}}">{{$row->id}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="moeda">{{ __('Moeda Par') }}</label> 
                            </div>
                            <div class="col-md-8">
                                <select name="moedaPar" id="moedaPar" class="form-control"></select>
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Consultar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Resultado
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="card-body" id="resultado"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/js/cotacoes/consultar.js"></script>
@endsection