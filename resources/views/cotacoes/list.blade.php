@extends('layouts.cliente')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h5 pl-0 mb-0 text-gray-800">Histórico de Cotações</h1>
    </div>
    <div class="card shadow ml-0 mb-4">
      <div class="card-body">
        <div class="table-responsive overflow-x-inherit">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>											
                <th>Par</th>
                <th>Compra</th>
                <th>Venda</th>
                <th class="only-desktop">Variação</th>
                <th class="only-desktop">Porcentagem de Variação</th>
                <th class="only-desktop">Máximo</th>
                <th class="only-desktop">Mínimo</th>
                <th class="only-desktop text-center">Data Criação</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($list as $row)
                    <tr>
                        <td>{{$row->par}}</td>
                        <td>{{$row->compra}}</td>
                        <td>{{$row->venda}}</td>
                        <td class="only-desktop">{{$row->variacao}}</td>
                        <td class="only-desktop">{{$row->porcentagem_variacao}}</td>
                        <td class="only-desktop">{{$row->maximo}}</td>
                        <td class="only-desktop">{{$row->minimo}}</td>
                        <td class="only-desktop text-center">{{date('d/m/Y H:i:s', strtotime($row->created_at))}}</td>
                    </tr>
                @endforeach
            </tbody>
          </table>
          @if( !empty($list) )
          {{ $list->appends(\Request::except('page'))->render() }}
          @endif
        </div>
      </div>
    </div>
@endsection