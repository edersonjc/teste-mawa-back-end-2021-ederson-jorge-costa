<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mawa Cotações</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="/css/welcome.css?v02" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <div class="logo" style="max-height: 100%;">
                      <img src="/images/logo-mawa.png" style="">
                      @if (Route::has('login'))
                          <div class="">
                              @auth
                                  <a href="{{ url('/home') }}">Home</a>
                              @else
                                  <a href="{{ route('login') }}">Login</a>
          
                                  @if (Route::has('register'))
                                      <a href="{{ route('register') }}">Cadastre-se</a>
                                  @endif
                              @endauth
                          </div>
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
